UISOURCES = $(shell ls *.ui)
UIPYTHON  = $(patsubst %.ui, ui_%.py, $(UISOURCES))

all: $(UIPYTHON)

clean:
	rm -f *~

ui_%.py: %.ui
	pyuic5 $< -o $@


.PHONY: all clean
