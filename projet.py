from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication
import sys
import ui_projet

class ExampleApp(QtWidgets.QMainWindow, ui_projet.Ui_MainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self,parent)
        self.setupUi(self)
        self.incruster_Button.clicked.connect(self.f1)
        self.parcourir_fonds_Button.clicked.connect(self.f2)
        self.actionenregistrer.triggered.connect(self.enregistrer)
        self.actionenregistrer_sous.triggered.connect(self.enregistrer_sous)
        self.actionquitter.triggered.connect(self.close) # méthode prédéfinie

    ### les fonctions de rappel quand on active telle ou telle partie du GUI
    def f1(self):
        print("retirer le fond vert")
    def f2(self):
        print("on devrait parcourir des fichiers")
    def enregistrer(self):
        print("on devrait enregistrer")
    def enregistrer_sous(self):
        print("on devrait enregistrer sous un autre nom")

def main():
    app = QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
