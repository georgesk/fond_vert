Projet studio sur fond vert
===========================

But
---
Créer un outil graphique commode, pour choisir 

 * une image fixe ou une vidéo de décor d'une part,
 * une vidéo d'un personnage filmé sur fond vert d'autre part,
 
et lancer la fabrication d'une vidéo où le personnage est incrusté dans 
le décor.

Méthode de développement :
--------------------------

On utilise Qt5 "designer" pour gérer l'interface graphique utilisateur
(GUI) ; les éléments graphiques importants sont désignés par des identifiants
reconnaissables, par exemple on choisit `parcourir_fonds_Button` plutôt
que simplement `Button` ou `Button_5`, quand il s'agit d'un bouton qui
déclenchera l'action de parcourir les fichiers à choisir pour un décor.

Chaque fois que le fichier `projet.ui` est modifié grâce à designer,
on relance la fabrication du fichier Python définissant la GUI. On
peut utiliser deux méthodes :
 * `pyuic5 projet.ui`
 * `make` ; cette dernière méthode est préférable, car grâce au script dans
   le fichier `Makefile`, tout fichier `machin.ui` est compilé en un fichier
   `ui_machin.py`, donc ça marchera si on doit définir plus tard de nombreux
   widgets différents pour l'interface graphique.
   
Le fichier `projet.py`
----------------------

C'est le fichier principal. Quand on lance la commande `python3 projet.py`,
ça lance l'application interactive.
